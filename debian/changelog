crowdsec-firewall-bouncer (0.0.25-5) unstable; urgency=medium

  * Add a symlink to config/crowdsec-firewall-bouncer.service from
    debian/, and drop it from crowdsec.install, leaving it up to
    dh_installsystemd to choose where to install it (Closes: #1073624).

 -- Cyril Brulebois <cyril@debamax.com>  Sun, 14 Jul 2024 04:49:47 +0200

crowdsec-firewall-bouncer (0.0.25-4) unstable; urgency=high

  * Set minimal version for the golang-github-google-nftables-dev build
    dependency to ensure a working AddSet() function, i.e. no longer
    reversing byte order for IPv4 and IPv6 addresses at the nftables level
    on little-endian architectures (Closes: #1071248, See: #1071247).

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 21 May 2024 10:15:36 +0200

crowdsec-firewall-bouncer (0.0.25-3) unstable; urgency=medium

  * Fix failure to install if crowdsec is unpacked but not configured
    (Closes: #1036985):
     - If both cscli and /etc/crowdsec/config.yaml exist, register as
       previously, via `cscli bouncers add`.
     - If cscli exists but /etc/crowdsec/config.yaml doesn't, create an
       API key similarly to what cscli would do (32 hexadecimal digits),
       and queue registration in /var/lib/crowdsec/pending-registration.
  * Add ConditionPathExists=!/var/lib/crowdsec/pending-registration to the
    systemd unit accordingly, so that it's skipped until crowdsec's
    postinst deals with the pending-registration file, removes it, and
    starts the units.
  * Use mode 600 when creating crowdsec-firewall-bouncer.yaml.local, since
    it contains an API key.

 -- Cyril Brulebois <cyril@debamax.com>  Wed, 31 May 2023 18:57:41 +0200

crowdsec-firewall-bouncer (0.0.25-2) unstable; urgency=medium

  * Stop shipping a logrotate configuration snippet, as the bouncer rotates
    logs on its own via lumberjack.Logger, and that can be configured in
    /etc/crowdsec/bouncers/crowdsec-firewall-bouncer.yaml (or the .local
    override):
     - Delete debian/crowdsec-firewall-bouncer.logrotate
     - Tag /etc/logrotate.d/crowdsec-firewall-bouncer remove-on-upgrade
       via debian/crowdsec-firewall-bouncer.conffiles.
  * When purging the package, remove internally-rotated log files, in
    addition to removing main log file and the logrotate-generated ones.

 -- Cyril Brulebois <cyril@debamax.com>  Mon, 20 Mar 2023 23:27:07 +0100

crowdsec-firewall-bouncer (0.0.25-1) unstable; urgency=medium

  * New upstream release.

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 21 Feb 2023 15:21:26 +0100

crowdsec-firewall-bouncer (0.0.25~rc3-2) unstable; urgency=medium

  * Fix missing license information for koneu/natend. Thanks, Thorsten
    Alteholz!

 -- Cyril Brulebois <cyril@debamax.com>  Sat, 18 Feb 2023 20:21:22 +0100

crowdsec-firewall-bouncer (0.0.25~rc3-1) unstable; urgency=medium

  * Initial release (Closes: #1025256).

 -- Cyril Brulebois <cyril@debamax.com>  Fri, 17 Feb 2023 21:04:53 +0100
