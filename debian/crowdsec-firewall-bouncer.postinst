#!/bin/sh
set -e

CONFIG=/etc/crowdsec/bouncers/crowdsec-firewall-bouncer.yaml
PENDING=/var/lib/crowdsec/pending-registration

# Create a configuration override file only once, see README.Debian:
if [ "$1" = configure ] && [ ! -f "$CONFIG.local" ]; then
  # Bail early if the Recommends on crowdsec isn't met:
  if ! which cscli >/dev/null 2>&1; then
    echo "W: cscli not found, no automatic registration" >&2
  else
    # Generate a unique identifier, since a given API can deal with several
    # machines; reuse the logic from crowdsec's postinst:
    unique=$(tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w 32 | head -n 1)
    id="FirewallBouncer-$unique"

    # crowdsec might be unpacked but not configured (#1036985):
    if [ -f /etc/crowdsec/config.yaml ]; then
      api_key=$(cscli --error -oraw bouncers add "$id")
      if [ -z "$api_key" ]; then
        echo "E: Local registration failed to yield an API key" >&2
        exit 1
      fi
    else
      api_key=$(tr -dc 'a-f0-9' < /dev/urandom | fold -w 32 | head -n 1)
      if [ ! -f $PENDING ]; then
        touch $PENDING
        chmod 600 $PENDING
      fi
      echo "crowdsec-firewall-bouncer $id $api_key" >> $PENDING
    fi

    # Store it so that it can be unregistered when purging the package:
    echo "$id" > "$CONFIG.id"
  fi

  # Logic and rationale detailed in README.Debian:
  alternative=$(update-alternatives --query iptables 2>/dev/null|awk '/^Value: / {print $2}')
  if [ "$alternative" = /usr/sbin/iptables-legacy ]; then
    if ! which ipset >/dev/null 2>&1; then
      echo 'W: Configuring iptables (iptables-legacy detected) but ipset is missing [see README.Debian]' >&2
    else
      echo 'I: Configuring iptables (iptables-legacy detected) [see README.Debian]' >&2
    fi
    firewall=iptables
  else
    echo 'I: Configuring nftables [see README.Debian]' >&2
    firewall=nftables
  fi

  touch "$CONFIG.local"
  chmod 600 "$CONFIG.local"

  # Generate the override:
  if [ -n "$api_key" ]; then
    cat > "$CONFIG.local" <<EOF
mode: $firewall
api_key: $api_key
EOF
  else
    cat > "$CONFIG.local" <<EOF
mode: $firewall
# non-local crowdsec setup: please specify both api_url and api_key manually
EOF
  fi
  echo "To adjust the config: editor $CONFIG.local && systemctl restart crowdsec-firewall-bouncer" >&2
fi

##DEBHELPER##
